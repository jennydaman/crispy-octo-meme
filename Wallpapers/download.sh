#!/usr/bin/env bash
# usage: ./download.sh < urls.txt

sed 's/#.*//' \
  | sed '/^ *$/d' \
  | parallel "$@" '
filename="$(awk "{print \$1}" <<< {})"
uri="$(awk "{print \$2}" <<< {})"
if [ -e "$filename" ]; then
  echo "$(tput dim)- $filename$(tput sgr0)"
else
  mkdir -p "$(dirname "$filename")"
  wget --quiet -O "$filename" "$uri"
  code=$?
  if [ "$code" = '0' ]; then
    symbol="$(tput setaf 2)✓"
  else
    symbol="$(tput setaf 1)✗"
  fi
  echo "$symbol$(tput sgr0) $filename $(tput dim)<-- $uri$(tput sgr0)"
  exit $code
fi
'
