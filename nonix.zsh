# ZSH configuration for non-NixOS systems

_zsh_plugins=(
  # pacman -S zsh-syntax-highlighting zsh-autosuggestions
  /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
  /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
  # apt install zsh-syntax-highlighting
  /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
)
for _zsh_plugin in "${_zsh_plugins[@]}"; do
  if [ -f "$_zsh_plugin" ]; then
    source "$_zsh_plugin"
  fi
done

# up arrow key search through history based on cursor position
# https://wiki.archlinux.org/index.php/zsh#History_search
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

[[ -n "${key[Up]}"   ]] && bindkey -- "${key[Up]}"   up-line-or-beginning-search
[[ -n "${key[Down]}" ]] && bindkey -- "${key[Down]}" down-line-or-beginning-search
