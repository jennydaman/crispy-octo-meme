if which ruby > /dev/null 2>&1; then
  export PATH=$PATH:$(ruby -e 'puts Gem.user_dir')/bin
fi

export PATH="$HOME/bin:$HOME/.local/bin:$HOME/.cargo/bin:/usr/lib/ccache/bin:$PATH:$HOME/.yarn/bin:/home/jenni/.conda/envs/datalad/bin"
export SUDO_EDITOR=hx
export VISUAL=hx
export EDITOR=hx

