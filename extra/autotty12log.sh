#!/bin/bash -ex

drop_in=/etc/systemd/journald.conf.d
mkdir $drop_in

cat >> $drop_in/fw-tty12.conf << EOF
[Journal]
ForwardToConsole=yes
TTYPath=/dev/tty12
MaxLevelConsole=info
EOF

systemctl restart systemd-journald

