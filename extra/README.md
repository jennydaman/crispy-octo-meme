# GRUB2 theme: 
1. `tar xvf gfxB-grub-theme.tar.gz -C /boot/grub/themes`
2. Edit `/etc/default/grub`: `GRUB_THEME="/boot/grub/themes/Breeze/theme.txt"`

Forked from https://www.gnome-look.org/p/1000111/. Patched the fonts and icons. 

