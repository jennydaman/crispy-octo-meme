# System environment variables
################################################################################

# ISO-8601 time stamps
export LC_TIME=en_DK.UTF-8

# Eye candy environment variables
################################################################################

# https://github.com/Qix-/better-exceptions
export BETTER_EXCEPTIONS=1

# ZSH options
################################################################################

# completion waiting dots
# https://github.com/ohmyzsh/ohmyzsh/blob/cf347ef3e43eabde5ae86e17d25690ebbeef5e6b/lib/completion.zsh#L61
expand-or-complete-with-dots() {
  print -Pn "%F{red}...%f"
  zle expand-or-complete
  zle redisplay
}
zle -N expand-or-complete-with-dots
# Set the function as the default tab completion widget
bindkey -M emacs "^I" expand-or-complete-with-dots
bindkey -M viins "^I" expand-or-complete-with-dots
bindkey -M vicmd "^I" expand-or-complete-with-dots

# show options when pressing tab, select first option
# after pressing tab a second time
setopt nomenucomplete

# type space before a commend to prevent it from being saved to history
setopt histignorespace

# don't pushd when using normal cd
unsetopt autopushd

_home_manager_session_vars="$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh"
if [ -f "$_home_manager_session_vars" ]; then
  source "$_home_manager_session_vars"
fi
unset _home_manager_session_vars

if [ "$(which zsh)" != '/run/current-system/sw/bin/zsh' ]; then
  # non-NixOS specific customizations
  source ~/.dotfiles/nonix.zsh
fi

# if running in getty1 or SSH
# if [ "$(tty)" = "/dev/tty1" -o -n "$SSH_TTY" ]; then
#   motd
# fi


# Package managers
################################################################################

# pnpm
export PNPM_HOME="/home/jenni/.local/share/pnpm"
case ":$PATH:" in
  *":$PNPM_HOME:"*) ;;
  *) export PATH="$PNPM_HOME:$PATH" ;;
esac
# pnpm end


# Functions and MOTD
################################################################################

# blocking function to wait for internet connection
# polls NetworkManager every half second, gives up after 5 seconds
# TODO rewrite using systemd network-online.target
function wait_for_network () {
  local polls=0
  until nmcli g | grep -qG "connected\s*full"; do
    sleep 0.5    # poll twice per second
    ((polls++))
    # give up after 5 seconds
    if [ "$polls" -gt 10 ]; then
      return 1
    fi
  done
}

function wttr () {
  local request="https://wttr.in/${1-Boston}?m&M&2"
  [ "$(tput cols)" -lt 125 ] && request+='&n'
  wait_for_network || return $?
  curl -H "Accept-Language: ${LANG%_*}" --compressed "$request"
}

function wttr_if_connected () {
  if nmcli g | grep -qE '^connected  full'; then
    wttr
  fi
}

function pigeon () {
  source ~/.dotfiles/.env
  export PIGEON_TITLE=y
  cd ~/.dotfiles/PigeonStats
  ./usage.sh $MOUNTED_PARTITIONS
  echo
  ./battery.sh
  echo
  ./temperature.sh cpu
  echo
  ./processes.sh
  cd - > /dev/null
}

alias Hyprland='Hyprland -c ~/.config/hypr/hosts/$(hostname).conf'

# print the date, surrounded by a rainbow border.
if which toilet > /dev/null 2>&1; then
  toilet -f term -F border --gay "$(date)"
fi
