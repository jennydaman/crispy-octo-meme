# Jennings Zhang's personal dotfiles!

Uses the [dotbot](https://github.com/anishathalye/dotbot) dotfiles helper. 

![Screenshot of zsh](extra/zsh-ss.png)

**Installation** (of the dotfiles): `git clone --recurse-submodules -j8 https://github.com/jennydaman/crispy-octo-meme.git ~/.dotfiles && ~/.dotfiles/install`

## shell dependencies

```sh
sudo pacman -S --needed zsh grml-zsh-config zsh-syntax-highlighting zsh-autosuggestions zsh-theme-powerlevel10k
sudo pacman -S --needed lsd bat diff-so-fancy
paru --aur -S --needed nerd-fonts-hack toilet
```

## Configurations

* set terminal font as "Hack Nerd Font Regular"
* `sudo visudo`: 

```
Defaults    insults
Defaults    lecture = always
Defaults    lecture_file = /root/sudoers.lecture
```


## Additional configurations for GNU/Linux installations: 

* https://wiki.archlinux.org/index.php/Systemd#Forward_journald_to_.2Fdev.2Ftty12
* https://wiki.archlinux.org/index.php/Getty#Have_boot_messages_stay_on_tty1
* https://www.reddit.com/r/archlinux/comments/6vez44/a_small_tip_if_you_compile_from_aur
* Increase tty scrollback buffer --- kernel parameter: `fbcon=scrollback:256k`

## desktop


### GNOME shell extensions

- Applications overview tooltip
- Coverflow alt-tab
- Gpaste
- Keys indicator
- Openweather
- System-monitor
- Windownavigator
- Freon

### pscircle

https://gitlab.com/mildlyparallel/pscircle

```sh
* * * * * pscircle --output=$HOME/Pictures/pscircle.png --background-image=$HOME/Pictures/Wallpapers/4k\ bot\ party\ dark\ 16x9.png --max-children=32 --dot-radius=4 --dot-border-width=1 --tree-font-size=24 --tree-radius-increment=144 --toplists-font-size=32 --toplists-row-height=40 --toplists-column-padding=20 --cpulist-center=700:-200 --memlist-center=700:200 --output-width=3840 --output-height=2160 # JOB_ID_1
```

## stuff to install

```bash
# fun stuff
sudo pacman -S fortune-mod cowsay lolcat doge cmatrix sl asciiquarium aalib htop gtop
# gnome
sudo pacman -S cheese eog epiphany evince evolution file-roller gedit gnome-backgrounds gnome-boxes gnome-calculator gnome-characters gnome-disk-utility gnome-todo gnome-tweaks gnome-usage gvfs gvfs-afc gvfs-goa gvfs-google gvfs-gphoto2 gvfs-mtp gvfs-nfs gvfs-smb nautilus seahorse
```

## Sources

- sudo woodo from https://gist.github.com/knpwrs/e6bde609123a748a5b94
- color palette for ANSI terminal: https://www.reddit.com/r/unixporn/comments/hjzw5f/oc_qualitative_color_palette_for_ansi_terminal/fwq8kis/

## Irrelevant

> times are changing, my friend.

* https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface#Mount_efivarfs
* "Bee" sudoers lecture from https://github.com/brianclemens/dotfiles/blob/master/sudoers.lecture
