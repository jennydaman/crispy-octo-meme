#!/usr/bin/env bun

import { spawnSync } from "child_process";

const ports = [
  3000, 4005, 5173, 5174, 7865, 8000, 8010, 8020, 8080, 8888, 25173,
  32000, 32001, 32002, 32003, 32004, 32005, 32006, 32007,
  32008, 32009, 32010, 32020
];
const tunnelArgs = ports.flatMap((n) => ["-L", `localhost:${n}:localhost:${n}`]);
const sshArgs = process.argv.splice(2);
const args = tunnelArgs.concat(sshArgs);

const proc = spawnSync("ssh", args, { stdio: "inherit" });
process.exit(proc.status === null ? 1 : proc.status);
