# Getting Started

    mkdir ~/.config/neomutt/login/account

Create one file per account in the folder `account/` e.g.

    account
    ├── bch
    ├── jenningszh
    └── neu

## How It Works

The custom script [neomutt-account-launcher](../../bin/neomutt-account-launcher),
which you should install to your `$PATH` as `neomutt` (and `alias mutt=neomutt`),
will source your account file when you run the command

    neomutt accountname

## Examples

### Gmail

```
# vim: filetype=muttrc
source ~/.config/neomutt/login/fresh

# create a login token from here
# https://myaccount.google.com/apppasswords
# It is used where 2FA is not possible.
set my_gmail_pass = "xxxxxxxxxxxxxxxx"

set from="Jennings Zhang <jz@customdomain.example.com>"
set imap_user="gmail_username@gmail.com"
alternates "^gmail_username@gmail\.com$" "^jz@customdomain\.example\.com$"
set pgp_default_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

set folder = "imaps://gmail_username%40gmail.com@imap.gmail.com/"
set smtp_url="smtps://gmail_username@smtp.gmail.com"

source ~/.config/neomutt/login/provider/gmail
account-hook $folder "set imap_user=gmail_username@gmail.com imap_pass=$my_gmail_pass"
```

### Regular IMAP/SMTP

Working with the _Boston Children's Hospital_ Outlook Exchange server.

```
# vim: filetype=muttrc

source ~/.config/neomutt/login/fresh
set my_bch_password = "xxxxxxxxxxxx"

set from="Jennings Zhang <Me@example.com>"
set imap_user="username"
alternates "^dev@babymri\.org$"

set folder = "imaps://username@mailserver.example.com:993"
set smtp_url = "smtp://username@mailserver.example.com:587"

source ~/.config/neomutt/login/provider/bch
account-hook $folder "set imap_user=username imap_pass=$my_bch_password"
```


## Password Storage

It may be desirable to set the passwords by themselves in other files
to separate account config from secrets. For example, I have

    account
    ├── bch
    ├── jenningszh
    ├── neu
    └── secret
        ├── bch
        └── jenningszh

In `account/jenningszh` there is a line

```
source ~/.config/neomutt/login/account/secret/jenningszh
```

And the content of `account/secret/jenningszh` looks like

```
set my_gmail_pass = "xxxxxxxxxxxxxxxx"
```

I use full-disk encryption, which is sufficient security for my threat model.
The community's recommendation is to use GPG. See
https://wiki.archlinux.org/title/Mutt#Passwords_management
