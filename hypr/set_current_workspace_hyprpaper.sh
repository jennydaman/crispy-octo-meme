#!/usr/bin/env bash
# Detect which workspace and monitor is currently active, and
# change the wallpaper to a preloaded image from hyprpaper.conf

activeworkspace="$(hyprctl activeworkspace -j)"
workspace="$(jq -r .id <<< "$activeworkspace")"
monitor="$(jq -r .monitor <<< "$activeworkspace")"
wallpaper="$(grep -oP '(?<=preload = ).+\.jpg' ~/.config/hypr/hyprpaper.conf | sed "${workspace}q;d")"

set -ex
exec hyprctl hyprpaper wallpaper "$monitor,$wallpaper"
