#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export VISUAL=vim
export EDITOR=$VISUAL
export SUDO_EDITOR=rvim

export PATH="$PATH:$HOME/bin:/home/jenni/.conda/envs/datalad/bin"

if [ -f /usr/share/gentoo-bashrc/bashrc ]; then
  source /usr/share/gentoo-bashrc/bashrc
  PS1='$(EXIT="$?"; [ "$EXIT" -ne "0" ] && echo "\[\e[31;1m\]$EXIT|")'$PS1
else
  PS1='$(EXIT="$?"; [ "$EXIT" -ne "0" ] && echo "\[\e[31;1m\]$EXIT|")$([ "$EUID" -eq 0 ] && user_color="\[\e[31;1m\]" || user_color="\[\e[1;35m\]";echo "$user_color")\u\[\033[00m\]@\h\[\033[00m\]:\[\033[34m\]\w\[\033[00m\]\$'
fi


